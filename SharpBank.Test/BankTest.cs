﻿using NUnit.Framework;
using System;
using SharpBank.Timer;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-10;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new CheckingAccount());
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CustomerSummary2()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new CheckingAccount());
            john.OpenAccount(new SavingAccount());
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (2 accounts)", bank.CustomerSummary());
        }

        [Test]
        public void WrongDeposit()
        {
            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(checkingAccount);
            bank.AddCustomer(bill);
            Assert.Throws<ArgumentException>(() => checkingAccount.Deposit(-1000), "amount must be greater than zero");
        }

        [Test]
        public void WrongWithdraw()
        {
            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(checkingAccount);
            bank.AddCustomer(bill);
            Assert.Throws<ArgumentException>(() => checkingAccount.Withdraw(-1000), "amount must be greater than zero");
        }

        [Test]
        public void CheckingAccount()
        {
            var timer = Time.GetInstance();
            timer.SetTestMode();
            var startDate = new DateTime(2019, 1, 1);
            timer.SetDate(startDate);

            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(checkingAccount);
            bank.AddCustomer(bill);
            checkingAccount.Deposit(1000);

            timer.SetDate(startDate.AddDays(365));
            double interest = (double)bank.TotalInterestPaid();
            Assert.AreEqual(1.0, interest, DOUBLE_DELTA);
            timer.SetNormalMode();
        }

        [Test]
        public void SavingsAccount()
        {
            var timer = Time.GetInstance();
            timer.SetTestMode();
            var startDate = new DateTime(2019, 1, 1);
            timer.SetDate(startDate);

            Bank bank = new Bank();
            Account savingAccount = new SavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingAccount);
            bank.AddCustomer(bill);
            savingAccount.Deposit(1000);

            timer.SetDate(startDate.AddDays(365));
            double interest = (double)bank.TotalInterestPaid();
            Assert.AreEqual(1.0, interest, DOUBLE_DELTA);
            timer.SetNormalMode();
        }

        [Test]
        public void SavingsAccount2()
        {
            var timer = Time.GetInstance();
            timer.SetTestMode();
            var startDate = new DateTime(2019, 1, 1);
            timer.SetDate(startDate);

            Bank bank = new Bank();
            Account savingAccount = new SavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingAccount);
            bank.AddCustomer(bill);
            savingAccount.Deposit(1500);

            timer.SetDate(startDate.AddDays(365));
            double interest = (double)bank.TotalInterestPaid();
            Assert.AreEqual(2.0, interest, DOUBLE_DELTA);
            timer.SetNormalMode();
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            var timer = Time.GetInstance();
            timer.SetTestMode();
            var startDate = new DateTime(2019, 1, 1);
            timer.SetDate(startDate);

            Bank bank = new Bank();
            Account maxiSavingAccount = new MaxiSavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(maxiSavingAccount);
            bank.AddCustomer(bill);
            maxiSavingAccount.Deposit(1000);

            timer.SetDate(startDate.AddDays(365));
            double interest = (double)bank.TotalInterestPaid();
            Assert.AreEqual(50.0, interest, DOUBLE_DELTA);
            timer.SetNormalMode();
        }

        [Test]
        public void MaxiSavingsAccount2()
        {
            var timer = Time.GetInstance();
            timer.SetTestMode();
            var startDate = new DateTime(2019, 1, 1);
            timer.SetDate(startDate);

            Bank bank = new Bank();
            Account maxiSavingAccount = new MaxiSavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(maxiSavingAccount);
            bank.AddCustomer(bill);
            maxiSavingAccount.Deposit(1000);

            timer.SetDate(startDate.AddDays(355));
            maxiSavingAccount.Withdraw(100);

            timer.SetDate(startDate.AddDays(365));
            var interest1 = 1000 * (0.05 * 355 / 365);
            var interest2 = (900 + interest1) * (0.001 * 10 / 365);

            double interest = (double)bank.TotalInterestPaid();
            Assert.AreEqual(interest1 + interest2, interest, DOUBLE_DELTA);
            timer.SetNormalMode();
        }

        [Test]
        public void OwnTransferAccounts()
        {
            Bank bank = new Bank();
            Account savingAccount = new SavingAccount();
            Account maxiSavingAccount = new MaxiSavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingAccount);
            bill.OpenAccount(maxiSavingAccount);
            bank.AddCustomer(bill);

            savingAccount.Deposit(3000);
            bill.OwnTransferAccount(savingAccount, maxiSavingAccount, 500);
            double savingAccountAmount = (double)savingAccount.SumTransactions();
            double maxiSavingAccountAmount = (double)maxiSavingAccount.SumTransactions();
            Assert.AreEqual(2500, savingAccountAmount, DOUBLE_DELTA);
            Assert.AreEqual(500, maxiSavingAccountAmount, DOUBLE_DELTA);
        }

        [Test]
        public void OwnTransferAccountsInvalidAmount()
        {
            Bank bank = new Bank();
            Account savingAccount = new SavingAccount();
            Account maxiSavingAccount = new MaxiSavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingAccount);
            bill.OpenAccount(maxiSavingAccount);
            bank.AddCustomer(bill);

            savingAccount.Deposit(3000);
            Assert.Throws<ApplicationException>(() => bill.OwnTransferAccount(savingAccount, maxiSavingAccount, 3500), "there is no enough available amount on the account");
        }

        [Test]
        public void OwnTransferAccountsInvalidTarget()
        {
            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount();
            Account savingAccount = new SavingAccount();
            Account maxiSavingAccount = new MaxiSavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingAccount);
            bill.OpenAccount(maxiSavingAccount);
            bank.AddCustomer(bill);

            savingAccount.Deposit(3000);
            Assert.Throws<ApplicationException>(() => bill.OwnTransferAccount(savingAccount, checkingAccount, 3500), "The target account is invalid");
        }

        [Test]
        public void OwnTransferAccountsInvalidSource()
        {
            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount();
            Account savingAccount = new SavingAccount();
            Account maxiSavingAccount = new MaxiSavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingAccount);
            bill.OpenAccount(maxiSavingAccount);
            bank.AddCustomer(bill);

            savingAccount.Deposit(3000);
            Assert.Throws<ApplicationException>(() => bill.OwnTransferAccount(checkingAccount, savingAccount, 3500), "The source account is invalid");
        }

        [Test]
        public void OwnTransferAccountsNegativeAmount()
        {
            Bank bank = new Bank();
            Account savingAccount = new SavingAccount();
            Account maxiSavingAccount = new MaxiSavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingAccount);
            bill.OpenAccount(maxiSavingAccount);
            bank.AddCustomer(bill);

            savingAccount.Deposit(3000);
            Assert.Throws<ArgumentException>(() => bill.OwnTransferAccount(savingAccount, maxiSavingAccount, -3500), "The transferred amount must be positive");
        }

        [Test]
        public void WithdrawalIncludingInterest()
        {
            var timer = Time.GetInstance();
            timer.SetTestMode();
            var startDate = new DateTime(2019, 1, 1);
            timer.SetDate(startDate);

            Bank bank = new Bank();
            Account savingAccount = new SavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingAccount);
            bank.AddCustomer(bill);
            savingAccount.Deposit(2000);

            timer.SetDate(startDate.AddDays(365));
            Assert.DoesNotThrow(() => savingAccount.Withdraw(2002));
            double interest = (double)bank.TotalInterestPaid();
            Assert.AreEqual(3.0, interest, DOUBLE_DELTA);
            timer.SetNormalMode();
        }

        [Test]
        public void WithdrawalIncludingInterest2()
        {
            var timer = Time.GetInstance();
            timer.SetTestMode();
            var startDate = new DateTime(2019, 1, 1);
            timer.SetDate(startDate);

            Bank bank = new Bank();
            Account savingAccount = new SavingAccount();
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingAccount);
            bank.AddCustomer(bill);
            savingAccount.Deposit(2000);

            timer.SetDate(startDate.AddDays(365));
            Assert.Throws<ApplicationException>(() => savingAccount.Withdraw(2005), "there is no enough available amount on the account");

            double interest = (double)bank.TotalInterestPaid();
            Assert.AreEqual(3.0, interest, DOUBLE_DELTA);
            timer.SetNormalMode();
        }
    }
}
