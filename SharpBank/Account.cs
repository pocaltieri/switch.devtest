﻿using SharpBank.InterestCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public abstract class Account
    {
        protected readonly ICalculator interestCalculator;
        protected List<Transaction> transactions;
        public IList<Transaction> Transactions
        {
            get
            {
                return transactions.AsReadOnly();
            }
        }
        protected decimal Balance { get; set; }

        public DateTime LastUpdatedBalance { get; protected set; }

        public Account(ICalculator calculator)
        {
            transactions = new List<Transaction>();
            Balance = 0;
            LastUpdatedBalance = DateTime.MinValue;
            interestCalculator = calculator;
        }

        public void Deposit(decimal amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
                UpdateBalance();
            }
        }

        public void Withdraw(decimal amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }

            var t = new Transaction(-amount);
            try
            {
                transactions.Add(t);
                UpdateBalance();
            }
            catch
            {
                transactions.Remove(t);
                throw;
            }
        }

        public decimal InterestEarned()
        {
            UpdateBalance(DateProvider.GetInstance().Now());
            return Balance - SumTransactions();
        }

        protected virtual decimal InterestEarned(decimal amount, int days, DateTime date)
        {
            return interestCalculator.InterestEarned(amount, days);
        }

        protected void UpdateBalance()
        {
            var unProcessedTrans = Transactions.Where(t => t.TransactionDate > LastUpdatedBalance);
            foreach (var t in unProcessedTrans)
            {
                var days = (t.TransactionDate - LastUpdatedBalance).Days;
                var interest = InterestEarned(Balance, days, t.TransactionDate);
                if (Balance + interest + t.Amount < 0)
                    throw new ApplicationException("there is no enough available amount on the account");

                Balance += interest;
                Balance += t.Amount;
                LastUpdatedBalance = t.TransactionDate;
            }
        }

        protected void UpdateBalance(DateTime date)
        {
            if(LastUpdatedBalance < date)
            {
                var days = (date - LastUpdatedBalance).Days;
                LastUpdatedBalance = date;
                Balance += InterestEarned(Balance, days, date);
            }
        }

        public decimal SumTransactions()
        {
            decimal amount = 0;
            foreach (var t in Transactions)
                amount += t.Amount;
            return amount;
        }

        public abstract string GetAccountTypeName();
    }
}
