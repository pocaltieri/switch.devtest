﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> Customers { get; set; }

        public Bank()
        {
            Customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            Customers.Add(customer);
        }

        public string CustomerSummary()
        {
            var summary = new StringBuilder();
            summary.Append("Customer Summary");
            foreach (var c in Customers)
            {
                summary.AppendFormat(string.Format("\n - {0} ({1})",c.Name, Format(c.GetNumberOfAccounts(), "account")));
            }
            return summary.ToString();
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        private string Format(int number, string word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        public decimal TotalInterestPaid()
        {
            decimal total = 0;
            foreach (var c in Customers)
                total += c.TotalInterestEarned();
            return total;
        }

        public string GetFirstCustomer()
        {
            if (Customers.Count == 0)
                throw new ApplicationException("There are no bank customers");
            return Customers.First().Name;
        }
    }
}
