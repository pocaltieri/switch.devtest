﻿using SharpBank.InterestCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class CheckingAccount : Account
    {
        private const decimal RATE = 0.001M;
        public CheckingAccount() : base(new CheckingAccountCalculator(RATE))
        {
        }

        public override string GetAccountTypeName()
        {
            return "Checking Account";
        }
    }
}
