﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Customer
    {
        public string Name { get; private set; }
        private List<Account> Accounts { get; set; }

        public Customer(string name)
        {
            this.Name = name;
            this.Accounts = new List<Account>();
        }

        public void OpenAccount(Account account)
        {
            Accounts.Add(account);
        }

        public int GetNumberOfAccounts()
        {
            return Accounts.Count;
        }

        public decimal TotalInterestEarned()
        {
            decimal total = 0;
            foreach (var a in Accounts)
                total += a.InterestEarned();
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public string GetStatement()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Statement for {0}\n", Name);
            decimal total = 0;
            foreach (var a in Accounts)
            {
                sb.AppendFormat("\n{0}\n", StatementForAccount(a));
                total += a.SumTransactions();
            }
            sb.AppendFormat("\nTotal In All Accounts {0}", ToDollars(total));
            return sb.ToString();
        }

        private String StatementForAccount(Account a)
        {
            var s = new StringBuilder();
            s.AppendFormat("{0}\n", a.GetAccountTypeName());

            //Now total up all the transactions
            decimal total = 0;
            foreach (var t in a.Transactions)
            {
                s.AppendFormat("  {0} {1}\n", t.Amount < 0 ? "withdrawal" : "deposit", ToDollars(t.Amount));
                total += t.Amount;
            }
            s.AppendFormat("Total {0}", ToDollars(total));
            return s.ToString();
        }

        private string ToDollars(decimal d)
        {
            return string.Format("${0:N2}", Math.Abs(d));
        }

        public void OwnTransferAccount(Account source, Account target, decimal amount)
        {
            if (!Accounts.Contains(source))
                throw new ApplicationException("The source account is invalid");
            if(!Accounts.Contains(target))
                throw new ApplicationException("The target account is invalid");
            if (amount <= 0)
                throw new ArgumentException("The transferred amount must be positive");

            source.Withdraw(amount);
            target.Deposit(amount);
        }
    }
}
