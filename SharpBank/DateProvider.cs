﻿using SharpBank.Timer;
using System;

namespace SharpBank
{
    public class DateProvider
    {
        private static DateProvider instance = null;

        public static DateProvider GetInstance()
        {
            if (instance == null)
            {
                instance = new DateProvider();
            }
            return instance;
        }

        public DateTime Now()
        {
            return Timer.Time.GetInstance().Now();
        }
    }
}
