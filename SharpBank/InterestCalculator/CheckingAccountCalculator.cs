﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculator
{
    public class CheckingAccountCalculator : ICalculator
    {
        public decimal Rate { get; private set; }

        public CheckingAccountCalculator(decimal rate)
        {
            Rate = rate;
        }

        public decimal InterestEarned(decimal amount, int days, bool existWithdraw = false)
        {
            return amount * InterestHelper.CalculateSimpleRate(Rate, days);
        }
    }
}
