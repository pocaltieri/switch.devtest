﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculator
{
    public interface ICalculator
    {
        decimal InterestEarned(decimal amount, int days, bool existWithdraw = false);
    }
}
