﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculator
{
    public class InterestHelper
    {
        public const int YEAR_DAYS = 365;

        public static decimal CalculateSimpleRate(decimal annualRate, int days)
        {
            return annualRate * days / YEAR_DAYS;
        }

        public static decimal CalculateCompoundedRate(decimal annualRate, int days)
        {
            var rate = Math.Pow((double)(1 + annualRate), days / YEAR_DAYS) - 1;
            return (decimal)rate;
        }
    }
}
