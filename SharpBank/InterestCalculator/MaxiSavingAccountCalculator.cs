﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculator
{
    public class MaxiSavingAccountCalculator : ICalculator
    {
        public decimal ThresholdLvl1 { get; private set; }
        public decimal ThresholdLvl2 { get; private set; }
        public decimal RateLvl1 { get; private set; }
        public decimal RateLvl2 { get; private set; }
        public decimal RateLvl3 { get; private set; }

        public MaxiSavingAccountCalculator(decimal thresholdLvl1, decimal thresholdLvl2, decimal rateLvl1, decimal rateLvl2, decimal rateLvl3)
        {
            ThresholdLvl1 = thresholdLvl1;
            ThresholdLvl2 = thresholdLvl2;
            RateLvl1 = rateLvl1;
            RateLvl2 = rateLvl2;
            RateLvl3 = rateLvl3;
        }

        public decimal InterestEarned(decimal amount, int days, bool existWithdraw = false)
        {
            decimal interestEarned = 0;
            var rate1 = InterestHelper.CalculateSimpleRate(RateLvl1, days);
            var rate2 = InterestHelper.CalculateSimpleRate(RateLvl2, days);
            var rate3 = InterestHelper.CalculateSimpleRate(RateLvl3, days);

            if (amount <= ThresholdLvl1)
            {
                interestEarned = amount * rate1;
            }
            else if (amount <= ThresholdLvl2)
            {
                interestEarned = ThresholdLvl1 * rate1 + (amount - ThresholdLvl1) * rate2;
            }
            else
            {
                interestEarned = ThresholdLvl1 * rate1 + ThresholdLvl2 * rate2 + (amount - ThresholdLvl2) * rate3;
            }
            
            return interestEarned;
        }
    }
}
