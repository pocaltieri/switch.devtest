﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculator
{
    public class MaxiSavingAccountCalculator2 : ICalculator
    {
        public decimal RateLvl1 { get; private set; }
        public decimal RateLvl2 { get; private set; }

        public MaxiSavingAccountCalculator2(decimal rateLvl1, decimal rateLvl2)
        {
            RateLvl1 = rateLvl1;
            RateLvl2 = rateLvl2;
        }

        public decimal InterestEarned(decimal amount, int days, bool existWithdraw)
        {
            decimal interestEarned = 0;
            var rate1 = InterestHelper.CalculateSimpleRate(RateLvl1, days);
            var rate2 = InterestHelper.CalculateSimpleRate(RateLvl2, days);

            if (existWithdraw)
            {
                interestEarned = amount * rate2;
            }
            else
            {
                interestEarned = amount * rate1;
            }

            return interestEarned;
        }
    }
}
