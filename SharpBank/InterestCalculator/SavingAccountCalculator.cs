﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculator
{
    public class SavingAccountCalculator : ICalculator
    {
        public decimal Threshold { get; private set; }
        public decimal RateLvl1 { get; private set; }
        public decimal RateLvl2 { get; private set; }

        public SavingAccountCalculator(decimal threshold, decimal rateLvl1, decimal rateLvl2)
        {
            Threshold = threshold;
            RateLvl1 = rateLvl1;
            RateLvl2 = rateLvl2;
        }

        public decimal InterestEarned(decimal amount, int days, bool existWithdraw = false)
        {
            decimal interestEarned = 0;
            var rate1 = InterestHelper.CalculateSimpleRate(RateLvl1, days);
            var rate2 = InterestHelper.CalculateSimpleRate(RateLvl2, days);

            if (amount <= Threshold)
                interestEarned = amount * rate1;
            else
                interestEarned = Threshold * rate1 + (amount - Threshold) * rate2;

            return interestEarned;
        }
    }
}
