﻿using SharpBank.InterestCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class MaxiSavingAccount : Account
    {
        private const int THRESHOLDDAYS = 10;
        private const decimal RATELVL1 = 0.05M;
        private const decimal RATELVL2 = 0.001M;

        public MaxiSavingAccount() : base(new MaxiSavingAccountCalculator2(RATELVL1, RATELVL2))
        {
        }

        public override string GetAccountTypeName()
        {
            return "Maxi Savings Account";
        }

        protected override decimal InterestEarned(decimal amount, int days, DateTime date)
        {
            var existWithdrawal = Transactions.Any(tran => tran.Amount < 0 &&
                                                            tran.TransactionDate >= date.AddDays(-THRESHOLDDAYS) &&
                                                            tran.TransactionDate < date);

            return interestCalculator.InterestEarned(amount, days, existWithdrawal);
        }

        //protected override void UpdateBalance()
        //{
        //    var unProcessedTrans = Transactions.Where(t => t.TransactionDate > LastUpdatedBalance);
        //    foreach (var t in unProcessedTrans)
        //    {
        //        var days = (t.TransactionDate - LastUpdatedBalance).Days;
        //        var existWithdrawal = Transactions.Any(tran => tran.Amount < 0 && 
        //                                                        tran.TransactionDate >= t.TransactionDate.AddDays(-10) &&
        //                                                        tran.TransactionDate <= t.TransactionDate);

        //        if (existWithdrawal)
        //        {
        //            Balance += InterestEarned2(Balance, days);
        //        }
        //        else
        //        {
        //            Balance += InterestEarned1(Balance, days);    
        //        }

        //        Balance += t.Amount;
        //        LastUpdatedBalance = t.TransactionDate;
        //    }
        //}
    }
}
