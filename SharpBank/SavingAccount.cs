﻿using SharpBank.InterestCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class SavingAccount : Account
    {
        private const decimal THRESHOLD = 1000;
        private const decimal RATELVL1 = 0.001M;
        private const decimal RATELVL2 = 0.002M;

        public SavingAccount() : base(new SavingAccountCalculator(THRESHOLD, RATELVL1, RATELVL2))
        {
        }

        public override string GetAccountTypeName()
        {
            return "Savings Account";
        }
    }
}
