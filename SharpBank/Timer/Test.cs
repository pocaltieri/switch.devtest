﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Timer
{
    public class Test : ITimerMode
    {
        private DateTime CurrentDate { get; set; }
        public Test()
        {
            CurrentDate = DateTime.Now;
        }

        public void SetDate(DateTime date)
        {
            CurrentDate = date;
        }

        public DateTime Now()
        {
            return CurrentDate;
        }
    }
}
