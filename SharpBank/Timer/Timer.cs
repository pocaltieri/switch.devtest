﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Timer
{
    public class Time
    {
        private static Time instance = null;
        private static ITimerMode mode = null;

        private Time()
        {
            mode = new Normal();
        }

        public static Time GetInstance()
        {
            if (instance == null)
            {
                instance = new Time();
            }
            return instance;
        }

        public void SetTestMode()
        {
            mode = new Test();
        }

        public void SetNormalMode()
        {
            mode = new Normal();
        }

        public void SetDate(DateTime date)
        {
            if(mode is Test)
            {
                ((Test)mode).SetDate(date);
            }
        }

        public DateTime Now()
        {
            return mode.Now();
        }
    }
}
