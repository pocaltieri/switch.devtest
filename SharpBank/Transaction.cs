﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public decimal Amount { get; }

        public DateTime TransactionDate { get; }

        public Transaction(decimal amount)
        {
            Amount = amount;
            TransactionDate = DateProvider.GetInstance().Now();
        }
    }
}
